﻿app.directive('wikiLinkInterceptor', function() {
    return {
        restrict: 'E',
        link: function(scope, elm, attr) {
            elm.on('click', function($event) {
                if (!attr.ngClick) {
                    var wikiIndex = attr.href.indexOf("/wiki/");
                    var pageTitle = attr.href.substring(wikiIndex + 6);
                    scope.loadContent(pageTitle);
                    $event.preventDefault();
                    return false;
                }
            })
        }
    }
})