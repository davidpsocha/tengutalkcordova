﻿app.directive('wikiContent', ['$compile', '$timeout', function ($compile, $timeout) {
    return { 
        restrict: 'E', 
        scope: false,
        replace: false,
        link: function(scope, element, attrs) {
            scope.$watch('content', function() {
                element.html(scope.content);
                $compile(element.contents())(scope);
            });
        }
    }
  }])