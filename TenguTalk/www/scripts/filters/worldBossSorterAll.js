﻿app.filter('worldBossSorterAll', function() {
    return function(items, field) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {    
            if (a.timeRemaining.label == "Running: " && b.timeRemaining.label == "ETA: ") {
                return -1;
            }
            else if (a.timeRemaining.label == "ETA: " && b.timeRemaining.label == "Running: ") {
                return 1;
            }
            else {
                return a.timeRemaining.total > b.timeRemaining.total ? 1 : -1;
            }
        });
        return filtered;
    };
});