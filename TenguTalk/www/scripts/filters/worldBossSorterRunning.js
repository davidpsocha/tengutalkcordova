﻿app.filter('worldBossSorterRunning', function() {
    return function(items, field) {
        var filtered = [];
        angular.forEach(items, function(item) {
            if (item.timeRemaining.label == "Running: ") {
                filtered.push(item);
            }
        });
        filtered.sort(function (a, b) {    
            return a.timeRemaining.total > b.timeRemaining.total ? 1 : -1;
        });
        return filtered;
    };
});