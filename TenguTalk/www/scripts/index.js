﻿// ------------------ Cordova section -------------------------------------------------

// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in cordova-simulate or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
document.addEventListener('deviceready', onDeviceReady.bind(this), false);

var push;
function onDeviceReady() {
    // Handle the Cordova pause and resume events
    document.addEventListener( 'pause', onPause.bind( this ), false );
    document.addEventListener( 'resume', onResume.bind( this ), false );
};

function onPause() {
    // TODO: This application has been suspended. Save application state here.
};

function onResume() {
    // TODO: This application has been reactivated. Restore application state here.
};


// ------------------ Angular section -------------------------------------------------

var app = angular.module('tenguTalkApp', ['ngRoute', 'ngSanitize']);
app.controller('mainCtrl', ['$scope', '$rootScope', '$window', function ($scope, $rootScope, $window) {
    $scope.showNav = false;
    $scope.title = "title";

    $rootScope.$on("$routeChangeSuccess", function (event, next, current) {
        var path = next.$$route ? next.$$route.originalPath : '';
        switch (path) {
            case '/options':
                $scope.title = 'Options';
                break;
            case '/home':
                $scope.title = 'Home';
                break;
            case '/wiki':
                $scope.title = 'Wiki';
                break;
            case '/worldboss':
                $scope.title = 'World Bosses';
                break;
            case '/colorwars':
                $scope.title = 'Color Wars';
                break;
            default:
                $scope.title = '';
                break;
        }
    });

    $scope.toggleNav = function () {
        $scope.showNav = !$scope.showNav;
    }
}]);
app.config(function ($routeProvider) {

    $routeProvider
        .when('/home', {
            templateUrl: 'templates/home.html',
            controller: 'homeCtrl'
        })
        .when('/options', {
            templateUrl: 'templates/options.html',
            controller: 'optionsCtrl'
        })
        .when('/worldboss', {
            templateUrl: 'templates/worldboss.html',
            controller: 'worldBossCtrl'
        })
        .when('/colorwars', {
            templateUrl: 'templates/colorwars.html',
            controller: 'colorWarsCtrl'
        })
        .when('/wiki', {
            templateUrl: 'templates/wiki.html',
            controller: 'wikiCtrl'
        })
        .otherwise({ redirectTo: '/home' });
});