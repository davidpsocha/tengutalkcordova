﻿app.service('videosService', function() {

    this.colorWarsPlaylist = [
        {
            Id: "eAmlPRTntKc",
            Title: "1 - The Color Lie",
            Description: "Join Manny Moonstone on a new Guild Wars 2 series! In this intro video, he explains the basis of color science and is a general precursor to the rest of the series. If you enjoyed the video please check out \"The Secret Language of Color\" by Joann and Arielle Eckstut as it inspired this series."
        },
        {
            Id: "1s7gsVhX8jA",
            Title: "2 - The Sylvari and The Pale Tree",
            Description: "Join Manny Moonstone in episode 2 of the Color Wars series! This time we talk about what colors mean to Sylvari and an important twist to the Pale Tree! If you enjoyed the video please check out \"The Secret Language of Color\" by Joann and Arielle Eckstut as it inspired this series."
        },
        {
            Id: "YsYpB3Mu7l4",
            Title: "3 - The Branded and Glint",
            Description: "Join Manny Moonstone in the third installment of Color Wars! This time we talk about the Branded and speculate on their material composition. As always please leave a comment on whatever you think! We try and improve each episode and love feedback!"
        },
        {
            Id: "OBw3eRJzhQU",
            Title: "3 - Part 2 Feedback and Comments!",
            Description: "We review further suggestions and theories from people about what Glint and Kralkatorrik's minions could be made out of!"
        },
        {
            Id: "fn800DFj40g",
            Title: "4 - The Sulfur Quarry",
            Description: "How did the Sulfur Quarry get so yellow? What's so significant about this place? Join Manny for another investigation in map art in GW2!"
        },
        {
            Id: "Ow_Cgdyx8J4",
            Title: "5 - Shooting Stars",
            Description: "Join us to talk about the new Path of Fire map: Istan! We investigate the phenomenon of Kralkatite meteors and other related points of interest!"
        }
    ];
});