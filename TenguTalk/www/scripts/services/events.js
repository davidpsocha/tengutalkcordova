﻿app.service('eventsService', function() {

    this.DayCycle = {
        Dawn:
        {
            Start: { Hour: 0, Min: 25 },
            End: { Hour: 0, Min: 30 }
        },
        Day:
        {
            Start: { Hour: 0, Min: 30 },
            End: { Hour: 1, Min: 40 }
        },
        Dusk:
        {
            Start: { Hour: 1, Min: 40 },
            End: { Hour: 1, Min: 45 }
        },
        Night:
        {
            Start: { Hour: 1, Min: 45 },
            End: { Hour: 0, Min: 25 }
        }
    };

    // Core Tyria Bosses

    this.GolemMarkIIList = [            
        {
            Start: { Hour: 8, Min: 0 },
            End: { Hour: 8, Min: 15 }
        },
        {
            Start: { Hour: 11, Min: 0 },
            End: { Hour: 11, Min: 15 }
        },
        {
            Start: { Hour: 14, Min: 0 },
            End: { Hour: 14, Min: 15 }
        },
        {
            Start: { Hour: 17, Min: 0 },
            End: { Hour: 17, Min: 15 }
        },
        {
            Start: { Hour: 20, Min: 0 },
            End: { Hour: 20, Min: 15 }
        },
        {
            Start: { Hour: 23, Min: 0 },
            End: { Hour: 23, Min: 15 }
        },
        {
            Start: { Hour: 2, Min: 0 },
            End: { Hour: 2, Min: 15 }
        },
        {
            Start: { Hour: 5, Min: 0 },
            End: { Hour: 5, Min: 15 }
        }
    ];

    this.SvanirShamanList = [
        {
            Start: { Hour: 8, Min: 15 },
            End: { Hour: 8, Min: 30 }
        },
        {
            Start: { Hour: 10, Min: 15 },
            End: { Hour: 10, Min: 30 }
        },
        {
            Start: { Hour: 12, Min: 15 },
            End: { Hour: 12, Min: 30 }
        },
        {
            Start: { Hour: 14, Min: 15 },
            End: { Hour: 14, Min: 30 }
        },
        {
            Start: { Hour: 16, Min: 15 },
            End: { Hour: 16, Min: 30 }
        },
        {
            Start: { Hour: 18, Min: 15 },
            End: { Hour: 18, Min: 30 }
        },
        {
            Start: { Hour: 20, Min: 15 },
            End: { Hour: 20, Min: 30 }
        },
        {
            Start: { Hour: 22, Min: 15 },
            End: { Hour: 22, Min: 30 }
        },
        {
            Start: { Hour: 0, Min: 15 },
            End: { Hour: 0, Min: 30 }
        },
        {
            Start: { Hour: 2, Min: 15 },
            End: { Hour: 2, Min: 30 }
        },
        {
            Start: { Hour: 4, Min: 15 },
            End: { Hour: 4, Min: 30 }
        },
        {
            Start: { Hour: 6, Min: 15 },
            End: { Hour: 6, Min: 30 }
        }
    ];

    this.ClawOfJormagList = [            
        {
            Start: { Hour: 8, Min: 30 },
            End: { Hour: 8, Min: 45 }
        },
        {
            Start: { Hour: 11, Min: 30 },
            End: { Hour: 11, Min: 45 }
        },
        {
            Start: { Hour: 14, Min: 30 },
            End: { Hour: 14, Min: 45 }
        },
        {
            Start: { Hour: 17, Min: 30 },
            End: { Hour: 17, Min: 45 }
        },
        {
            Start: { Hour: 20, Min: 30 },
            End: { Hour: 20, Min: 45 }
        },
        {
            Start: { Hour: 23, Min: 30 },
            End: { Hour: 23, Min: 45 }
        },
        {
            Start: { Hour: 2, Min: 30 },
            End: { Hour: 2, Min: 45 }
        },
        {
            Start: { Hour: 5, Min: 30 },
            End: { Hour: 5, Min: 45 }
        }
    ];

    this.FireElementalList = [
        {
            Start: { Hour: 8, Min: 45 },
            End: { Hour: 9, Min: 00 }
        },
        {
            Start: { Hour: 10, Min: 45 },
            End: { Hour: 11, Min: 00 }
        },
        {
            Start: { Hour: 12, Min: 45 },
            End: { Hour: 13, Min: 00 }
        },
        {
            Start: { Hour: 14, Min: 45 },
            End: { Hour: 15, Min: 00 }
        },
        {
            Start: { Hour: 16, Min: 45 },
            End: { Hour: 17, Min: 00 }
        },
        {
            Start: { Hour: 18, Min: 45 },
            End: { Hour: 19, Min: 00 }
        },
        {
            Start: { Hour: 20, Min: 45 },
            End: { Hour: 21, Min: 00 }
        },
        {
            Start: { Hour: 22, Min: 45 },
            End: { Hour: 23, Min: 00 }
        },
        {
            Start: { Hour: 0, Min: 45 },
            End: { Hour: 1, Min: 00 }
        },
        {
            Start: { Hour: 2, Min: 45 },
            End: { Hour: 3, Min: 00 }
        },
        {
            Start: { Hour: 4, Min: 45 },
            End: { Hour: 5, Min: 00 }
        },
        {
            Start: { Hour: 6, Min: 45 },
            End: { Hour: 7, Min: 00 }
        }
    ];

    this.TaidhaCovingtonList = [
        {
            Start: { Hour: 9, Min: 00 },
            End: { Hour: 9, Min: 15 }
        },
        {
            Start: { Hour: 12, Min: 00 },
            End: { Hour: 12, Min: 15 }
        },
        {
            Start: { Hour: 15, Min: 00 },
            End: { Hour: 15, Min: 15 }
        },
        {
            Start: { Hour: 18, Min: 00 },
            End: { Hour: 18, Min: 15 }
        },
        {
            Start: { Hour: 21, Min: 00 },
            End: { Hour: 21, Min: 15 }
        },
        {
            Start: { Hour: 0, Min: 00 },
            End: { Hour: 0, Min: 15 }
        },
        {
            Start: { Hour: 3, Min: 00 },
            End: { Hour: 3, Min: 15 }
        },
        {
            Start: { Hour: 6, Min: 00 },
            End: { Hour: 6, Min: 15 }
        }
    ];

    this.GreatJungleWurmList = [
        {
            Start: { Hour: 9, Min: 15 },
            End: { Hour: 9, Min: 30 }
        },
        {
            Start: { Hour: 11, Min: 15 },
            End: { Hour: 11, Min: 30 }
        },
        {
            Start: { Hour: 13, Min: 15 },
            End: { Hour: 13, Min: 30 }
        },
        {
            Start: { Hour: 15, Min: 15 },
            End: { Hour: 15, Min: 30 }
        },
        {
            Start: { Hour: 17, Min: 15 },
            End: { Hour: 17, Min: 30 }
        },
        {
            Start: { Hour: 19, Min: 15 },
            End: { Hour: 19, Min: 30 }
        },
        {
            Start: { Hour: 21, Min: 15 },
            End: { Hour: 21, Min: 30 }
        },
        {
            Start: { Hour: 23, Min: 15 },
            End: { Hour: 23, Min: 30 }
        },
        {
            Start: { Hour: 1, Min: 15 },
            End: { Hour: 1, Min: 30 }
        },
        {
            Start: { Hour: 3, Min: 15 },
            End: { Hour: 3, Min: 30 }
        },
        {
            Start: { Hour: 5, Min: 15 },
            End: { Hour: 5, Min: 30 }
        },
        {
            Start: { Hour: 7, Min: 15 },
            End: { Hour: 7, Min: 30 }
        }
    ];

    this.MegadestroyerList = [
        {
            Start: { Hour: 9, Min: 30 },
            End: { Hour: 9, Min: 45 }
        },
        {
            Start: { Hour: 12, Min: 30 },
            End: { Hour: 12, Min: 45 }
        },
        {
            Start: { Hour: 15, Min: 30 },
            End: { Hour: 15, Min: 45 }
        },
        {
            Start: { Hour: 18, Min: 30 },
            End: { Hour: 18, Min: 45 }
        },
        {
            Start: { Hour: 21, Min: 30 },
            End: { Hour: 21, Min: 45 }
        },
        {
            Start: { Hour: 0, Min: 30 },
            End: { Hour: 0, Min: 45 }
        },
        {
            Start: { Hour: 3, Min: 30 },
            End: { Hour: 3, Min: 45 }
        },
        {
            Start: { Hour: 6, Min: 30 },
            End: { Hour: 6, Min: 45 }
        }
    ];

    this.ShadowBehemothList = [
        {
            Start: { Hour: 9, Min: 45 },
            End: { Hour: 10, Min: 00 }
        },
        {
            Start: { Hour: 11, Min: 45 },
            End: { Hour: 12, Min: 00 }
        },
        {
            Start: { Hour: 13, Min: 45 },
            End: { Hour: 14, Min: 00 }
        },
        {
            Start: { Hour: 15, Min: 45 },
            End: { Hour: 16, Min: 00 }
        },
        {
            Start: { Hour: 17, Min: 45 },
            End: { Hour: 18, Min: 00 }
        },
        {
            Start: { Hour: 19, Min: 45 },
            End: { Hour: 20, Min: 00 }
        },
        {
            Start: { Hour: 21, Min: 45 },
            End: { Hour: 22, Min: 00 }
        },
        {
            Start: { Hour: 23, Min: 45 },
            End: { Hour: 0, Min: 00 }
        },
        {
            Start: { Hour: 1, Min: 45 },
            End: { Hour: 2, Min: 00 }
        },
        {
            Start: { Hour: 3, Min: 45 },
            End: { Hour: 4, Min: 00 }
        },
        {
            Start: { Hour: 5, Min: 45 },
            End: { Hour: 6, Min: 00 }
        },
        {
            Start: { Hour: 7, Min: 45 },
            End: { Hour: 8, Min: 00 }
        }
    ];

    this.ShattererList = [
        {
            Start: { Hour: 10, Min: 00 },
            End: { Hour: 10, Min: 15 }
        },
        {
            Start: { Hour: 13, Min: 00 },
            End: { Hour: 13, Min: 15 }
        },
        {
            Start: { Hour: 16, Min: 00 },
            End: { Hour: 16, Min: 15 }
        },
        {
            Start: { Hour: 19, Min: 00 },
            End: { Hour: 19, Min: 15 }
        },
        {
            Start: { Hour: 22, Min: 00 },
            End: { Hour: 22, Min: 15 }
        },
        {
            Start: { Hour: 1, Min: 00 },
            End: { Hour: 1, Min: 15 }
        },
        {
            Start: { Hour: 4, Min: 00 },
            End: { Hour: 4, Min: 15 }
        },
        {
            Start: { Hour: 7, Min: 00 },
            End: { Hour: 7, Min: 15 }
        }
    ];

    this.ModniirUlgothList = [
        {
            Start: { Hour: 10, Min: 30 },
            End: { Hour: 10, Min: 45 }
        },
        {
            Start: { Hour: 13, Min: 30 },
            End: { Hour: 13, Min: 45 }
        },
        {
            Start: { Hour: 16, Min: 30 },
            End: { Hour: 16, Min: 45 }
        },
        {
            Start: { Hour: 19, Min: 30 },
            End: { Hour: 19, Min: 45 }
        },
        {
            Start: { Hour: 22, Min: 30 },
            End: { Hour: 22, Min: 45 }
        },
        {
            Start: { Hour: 1, Min: 30 },
            End: { Hour: 1, Min: 45 }
        },
        {
            Start: { Hour: 4, Min: 30 },
            End: { Hour: 4, Min: 45 }
        },
        {
            Start: { Hour: 7, Min: 30 },
            End: { Hour: 7, Min: 45 }
        }
    ];

    this.KarkaQueenList = [
        {
            Start: { Hour: 10, Min: 30 },
            End: { Hour: 10, Min: 45 }
        },
        {
            Start: { Hour: 15, Min: 00 },
            End: { Hour: 15, Min: 15 }
        },
        {
            Start: { Hour: 18, Min: 10 },
            End: { Hour: 18, Min: 15 }
        },
        {
            Start: { Hour: 23, Min: 00 },
            End: { Hour: 23, Min: 15 }
        },
        {
            Start: { Hour: 2, Min: 00 },
            End: { Hour: 2, Min: 15 }
        },
        {
            Start: { Hour: 6, Min: 00 },
            End: { Hour: 6, Min: 15 }
        }
    ];

    this.TequatlList = [
        {
            Start: { Hour: 11, Min: 30 },
            End: { Hour: 11, Min: 45 }
        },
        {
            Start: { Hour: 16, Min: 00 },
            End: { Hour: 16, Min: 15 }
        },
        {
            Start: { Hour: 19, Min: 00 },
            End: { Hour: 19, Min: 15 }
        },
        {
            Start: { Hour: 00, Min: 00 },
            End: { Hour: 00, Min: 15 }
        },
        {
            Start: { Hour: 03, Min: 00 },
            End: { Hour: 03, Min: 15 }
        },
        {
            Start: { Hour: 08, Min: 00 },
            End: { Hour: 08, Min: 15 }
        }
    ];

    this.TripleTroubleList = [
        {
            Start: { Hour: 8, Min: 00 },
            End: { Hour: 8, Min: 15 }
        },
        {
            Start: { Hour: 12, Min: 30 },
            End: { Hour: 12, Min: 45 }
        },
        {
            Start: { Hour: 17, Min: 00 },
            End: { Hour: 17, Min: 15 }
        },
        {
            Start: { Hour: 20, Min: 00 },
            End: { Hour: 20, Min: 15 }
        },
        {
            Start: { Hour: 1, Min: 00 },
            End: { Hour: 1, Min: 15 }
        },
        {
            Start: { Hour: 4, Min: 00 },
            End: { Hour: 4, Min: 15 }
        }
    ];

    // Heart of Thorns Bosses

    this.NightAndTheEnemyList = [
        {
            Start: { Hour: 1, Min: 45 },
            End: { Hour: 2, Min: 25 }
        },
        {
            Start: { Hour: 3, Min: 45 },
            End: { Hour: 4, Min: 25 }
        },
        {
            Start: { Hour: 5, Min: 45 },
            End: { Hour: 6, Min: 25 }
        },
        {
            Start: { Hour: 7, Min: 45 },
            End: { Hour: 8, Min: 25 }
        },
        {
            Start: { Hour: 9, Min: 45 },
            End: { Hour: 10, Min: 25 }
        },
        {
            Start: { Hour: 11, Min: 45 },
            End: { Hour: 12, Min: 25 }
        },
        {
            Start: { Hour: 13, Min: 45 },
            End: { Hour: 14, Min: 25 }
        },
        {
            Start: { Hour: 15, Min: 45 },
            End: { Hour: 16, Min: 25 }
        },
        {
            Start: { Hour: 17, Min: 45 },
            End: { Hour: 18, Min: 25 }
        },
        {
            Start: { Hour: 19, Min: 45 },
            End: { Hour: 20, Min: 25 }
        },
        {
            Start: { Hour: 21, Min: 45 },
            End: { Hour: 22, Min: 25 }
        },
        {
            Start: { Hour: 23, Min: 45 },
            End: { Hour: 0, Min: 25 }
        }
    ];

    this.DefendingTarirList = [
        {
            Start: { Hour: 0, Min: 45 },
            End: { Hour: 1, Min: 30 }
        },
        {
            Start: { Hour: 2, Min: 45 },
            End: { Hour: 3, Min: 30 }
        },
        {
            Start: { Hour: 4, Min: 45 },
            End: { Hour: 5, Min: 30 }
        },
        {
            Start: { Hour: 6, Min: 45 },
            End: { Hour: 7, Min: 30 }
        },
        {
            Start: { Hour: 8, Min: 45 },
            End: { Hour: 9, Min: 30 }
        },
        {
            Start: { Hour: 10, Min: 45 },
            End: { Hour: 11, Min: 30 }
        },
        {
            Start: { Hour: 12, Min: 45 },
            End: { Hour: 13, Min: 30 }
        },
        {
            Start: { Hour: 14, Min: 45 },
            End: { Hour: 15, Min: 30 }
        },
        {
            Start: { Hour: 16, Min: 45 },
            End: { Hour: 17, Min: 30 }
        },
        {
            Start: { Hour: 18, Min: 45 },
            End: { Hour: 19, Min: 30 }
        },
        {
            Start: { Hour: 20, Min: 45 },
            End: { Hour: 21, Min: 30 }
        },
        {
            Start: { Hour: 22, Min: 45 },
            End: { Hour: 23, Min: 30 }
        }
    ];

    this.ChakGerentList = [
        {
            Start: { Hour: 0, Min: 25 },
            End: { Hour: 0, Min: 50 }
        },
        {
            Start: { Hour: 2, Min: 25 },
            End: { Hour: 2, Min: 50 }
        },
        {
            Start: { Hour: 4, Min: 25 },
            End: { Hour: 4, Min: 50 }
        },
        {
            Start: { Hour: 6, Min: 25 },
            End: { Hour: 6, Min: 50 }
        },
        {
            Start: { Hour: 8, Min: 25 },
            End: { Hour: 8, Min: 50 }
        },
        {
            Start: { Hour: 10, Min: 25 },
            End: { Hour: 10, Min: 50 }
        },
        {
            Start: { Hour: 12, Min: 25 },
            End: { Hour: 12, Min: 50 }
        },
        {
            Start: { Hour: 14, Min: 25 },
            End: { Hour: 14, Min: 50 }
        },
        {
            Start: { Hour: 16, Min: 25 },
            End: { Hour: 16, Min: 50 }
        },
        {
            Start: { Hour: 18, Min: 25 },
            End: { Hour: 18, Min: 50 }
        },
        {
            Start: { Hour: 20, Min: 25 },
            End: { Hour: 20, Min: 50 }
        },
        {
            Start: { Hour: 22, Min: 25 },
            End: { Hour: 22, Min: 50 }
        }
    ];
});