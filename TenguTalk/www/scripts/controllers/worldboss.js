﻿app.controller('worldBossCtrl', ['$scope', 'eventsService', '$interval', function ($scope, eventsService, $interval) {
    $scope.events = eventsService;
    $scope.masterList = [];
    $scope.maxWorldBosses = 15;

    $scope.getTimeRemaining = function (event) {
        var now = new Date(); 
        var nowUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
        var startUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  event.Start.Hour, event.Start.Min, 0);
        var endUTC = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  event.End.Hour, event.End.Min, 0);

        // If the event has passed, add one day to the schedule to get the correct time remaining.
        if(event.End.Hour < nowUTC.getHours() ||  (event.End.Hour == nowUTC.getHours() && event.End.Min < nowUTC.getMinutes())) 
        {
            startUTC.setDate(startUTC.getDate() + 1);
            endUTC.setDate(endUTC.getDate() + 1);
        }

        var startDiff = startUTC.getTime()/1000 - nowUTC.getTime()/1000;
        var eventStarted = startDiff < 0;
        var endDiff = endUTC.getTime()/1000 - nowUTC.getTime()/1000;
        var eventEnded = endDiff < 0;

        if(eventStarted && !eventEnded){
            var secondsLeft = Math.floor(endDiff);
            var minutesLeft = Math.floor(secondsLeft / 60);
            var hoursLeft = Math.floor(minutesLeft / 60);
            return { 
                total: secondsLeft,
                hours: hoursLeft,
                minutes: minutesLeft % 60,
                seconds: secondsLeft % 60,
                label: "Running: "
            };
        }
        else {
            var secondsLeft = Math.floor(startDiff);
            var minutesLeft = Math.floor(secondsLeft / 60);
            var hoursLeft = Math.floor(minutesLeft / 60);
            return { 
                total: secondsLeft,
                hours: hoursLeft,
                minutes: minutesLeft % 60,
                seconds: secondsLeft % 60,
                label: "ETA: "
            };
        }
    }

    $scope.populateMasterList = function () {
        for (var i = 0; i < $scope.events.GolemMarkIIList.length; i++) {
            var next = $scope.events.GolemMarkIIList[i];
            $scope.masterList.push({
                title: 'Golem Mark II',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/golemmark2_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.FireElementalList.length; i++) {
            var next = $scope.events.FireElementalList[i];
            $scope.masterList.push({
                title: 'Fire Elemental',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/fireelemental_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.SvanirShamanList.length; i++) {
            var next = $scope.events.SvanirShamanList[i];
            $scope.masterList.push({
                title: 'Svanir Shaman',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/svanirshaman_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.ClawOfJormagList.length; i++) {
            var next = $scope.events.ClawOfJormagList[i];
            $scope.masterList.push({
                title: 'Claw of Jormag',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/jormag_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.TaidhaCovingtonList.length; i++) {
            var next = $scope.events.TaidhaCovingtonList[i];
            $scope.masterList.push({
                title: 'Taidha Covington',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/thaidacovington_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.GreatJungleWurmList.length; i++) {
            var next = $scope.events.GreatJungleWurmList[i];
            $scope.masterList.push({
                title: 'Great Jungle Wurm',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/junglewurm_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.MegadestroyerList.length; i++) {
            var next = $scope.events.MegadestroyerList[i];
            $scope.masterList.push({
                title: 'Megadestroyer',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/megadestroyer_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.ShadowBehemothList.length; i++) {
            var next = $scope.events.ShadowBehemothList[i];
            $scope.masterList.push({
                title: 'Shadow Behemoth',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/shadowbehemoth_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.ShattererList.length; i++) {
            var next = $scope.events.ShattererList[i];
            $scope.masterList.push({
                title: 'Shatterer',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/shatterer_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.ModniirUlgothList.length; i++) {
            var next = $scope.events.ModniirUlgothList[i];
            $scope.masterList.push({
                title: 'Modniir Ulgoth',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/modinirulgoth_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.KarkaQueenList.length; i++) {
            var next = $scope.events.KarkaQueenList[i];
            $scope.masterList.push({
                title: 'Karka Queen',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/karkaqueen_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.TequatlList.length; i++) {
            var next = $scope.events.TequatlList[i];
            $scope.masterList.push({
                title: 'Tequatl',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/tequatl_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.TripleTroubleList.length; i++) {
            var next = $scope.events.TripleTroubleList[i];
            $scope.masterList.push({
                title: 'Triple Trouble',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/tripletrouble_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.NightAndTheEnemyList.length; i++) {
            var next = $scope.events.NightAndTheEnemyList[i];
            $scope.masterList.push({
                title: 'Night and the Enemy',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/nightandtheenemy_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.DefendingTarirList.length; i++) {
            var next = $scope.events.DefendingTarirList[i];
            $scope.masterList.push({
                title: 'Defending Tarir',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/savingtarir_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
        for (var i = 0; i < $scope.events.ChakGerentList.length; i++) {
            var next = $scope.events.ChakGerentList[i];
            $scope.masterList.push({
                title: 'Chak Gerent',
                img: cordova.file.applicationDirectory + 'www/images/BossLocations/chakgerent_small.jpg',
                schedule: next,
                timeRemaining: $scope.getTimeRemaining(next)
            });
        }
    }

    $scope.updateMasterList = function () {
        for (var i = 0; i < $scope.masterList.length; i++) {
            var event = $scope.masterList[i];
            event.timeRemaining = $scope.getTimeRemaining(event.schedule);
        }
    }

    $scope.populateMasterList();
    $interval($scope.updateMasterList, 1000);
    
}]);