﻿app.controller('colorWarsCtrl', ['$scope', 'videosService', '$interval', '$http', '$sce',
    function ($scope, videosService, $interval, $http, $sce) {

        $scope.apiKey = "AIzaSyApV6PIi1iWM392cA463C0Jr3BigHGuiws";
        $scope.playlistId = "PLJXsUjVPis1g1aikmACdawln97PWDNffl";
        $scope.selectedVideo = {};
        
        $scope.updateVideoPlayer = function () {
            $scope.selectedVideoUrl = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + $scope.selectedVideo.snippet.resourceId.videoId + "?enablejsapi=1");
        }

        $scope.load = function () {
            $http({
                method: 'GET',
                url: "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=" + $scope.playlistId + "&key=" + $scope.apiKey
            }).then(function success(res) {
                $scope.videos = res.data.items;
                if ($scope.videos.length > 0) {
                    $scope.selectedVideo = $scope.videos[$scope.videos.length - 1];
                    $scope.updateVideoPlayer();
                }
                
            }, function error(res) {
            
            });
        }
        $scope.load();
}]);