﻿app.controller('wikiCtrl', ['$scope', '$interval', '$http', '$timeout', '$window',
    function ($scope, $interval, $http, $timeout, $window) {
        $scope.content = "";

        $scope.prepareContent = function (input) {
            // First make sure all image references have absolute paths.
            var cleaned = input.replace(new RegExp("/images/", 'g'), "https://wiki.guildwars2.com/images/");

            // Remove any of the "[edit]" links from the dom. These come in a few forms.
            cleaned = cleaned.replace(new RegExp("<div class=\"plainlinks\".*</div>", 'g'), "");
            cleaned = cleaned.replace(new RegExp("<span class=\"mw-editsection\".*</span>", 'g'), "");

            // Change any href attributes to use ng-click events for faster UI responsiveness.
            cleaned = cleaned.replace(new RegExp("<a[^>]*href=\"[^\"]*\"", 'g'), function(value) {
                var wikiIndex = value.indexOf("/wiki/");
                var endQuote = value.indexOf("\"", wikiIndex);
                var pageTitle = value.substring(wikiIndex + 6, endQuote);
                var hrefIndex = value.indexOf("href=");

                var result = 
                    value.substring(0, hrefIndex) + 
                    " ng-click=\"loadContent('" + pageTitle + "')\"";
                return result;
            });

            // Finally, if we are over a reasonable page length remove any screenshot images and any small icons 
            // to help the UI not flake out.
            if (cleaned.length > 60000) {
                cleaned = cleaned.replace(new RegExp("<div style=\"overflow: hidden; width: 25px; height: 25px; display:inline-block; vertical-align:middle;", 'g'), function(value) {
                    return "<div style=\"overflow: hidden; width: 25px; height: 25px; display:none; vertical-align:middle;";    
                });
                cleaned = cleaned.replace(new RegExp("<span class=\"small item-icon thumb-icon\" style=\"display:inline-block;", 'g'), function(value) {
                    return "<span class=\"small item-icon thumb-icon\" style=\"display:none;";    
                });
                cleaned = cleaned.replace(new RegExp("<ul class=\"gallery mw-gallery-traditional\"", 'g'), function(value) {
                    return "<ul class=\"gallery mw-gallery-traditional\" style=\"display: none;\"";    
                });
            }

            
            return cleaned;
        }

        $scope.loadContent = function (title) {
            $http({
                method: 'GET',
                url: "https://wiki.guildwars2.com/api.php?action=parse&format=json&page=" + title
            }).then(function success(res) {
                var temp = res.data.parse.text["*"];
                $window.scrollTo(0, 0);
                $scope.content = $scope.prepareContent(temp);

            }, function error(res) {
                console.error(res);
            });
        }

        $scope.formatSearchResult = function (title) {
            return "<a href=\"/wiki/" + title.replace(new RegExp(" ", 'g'), "_") + "\">" + title + "</a><br/>"
        }

        $scope.search = function () {
            $http({
                method: 'GET',
                url: "https://wiki.guildwars2.com/api.php?action=opensearch&format=json&limit=10&suggest=true&search=" + encodeURI($scope.searchString)
            }).then(function success(res) {
                var results = res.data[1];
                var content = "";
                for (var i = 0; i < results.length; i++) {
                    content += $scope.formatSearchResult(results[i]);
                }
                $window.scrollTo(0, 0);
                $scope.content = $scope.prepareContent(content);

            }, function error(res) {
                console.error(res);
            });
        }

        $scope.searchKeyUp = function ($event) {
            if ($event.keyCode == 13) {
                $scope.search();
            }
        }

        $scope.loadContent("Main_Page");
    }
]);